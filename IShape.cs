﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Analiza7
{
    internal interface IShape
    {
        void Draw(Graphics g, Pen p, int x, int y);
    }

    class Circle : IShape
    {
        int r = 25;

        public void Draw(Graphics g, Pen p, int x, int y)
        {
            g.DrawEllipse(p, x - 10, y - 10, r, r);
        }
    }

    class X : IShape
    {
        public void Draw(Graphics g, Pen p, int x, int y)
        {
            g.DrawLine(p, x - 10, y + 10, x + 10, y - 10);
            g.DrawLine(p, x - 10, y - 10, x + 10, y + 10);
        }
    }

}