﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Analiza7
{
    public partial class Form1 : Form
    {
        int rez00 = 0, rez01 = 0, rez02 = 0, rez10 = 0, rez11 = 0, rez12 = 0, rez20 = 0, rez21 = 0, rez22 = 0;
        bool m00 = false, m01 = false, m02 = false, m10 = false, m11 = false, m12 = false, m20 = false, m21 = false, m22 = false;

        Graphics g00, g01, g02, g10, g11, g12, g20, g21, g22;

        

        bool nchg1 = false, nchg2 = false;
        bool finished = false;
        int O_cnt = 0, X_cnt = 0, D_cnt = 0;
        bool player = true;

        Pen p = new Pen(Color.Red, 1.0f);

        IShape s;

        public Form1()
        {
            InitializeComponent();
            g00 = pB00.CreateGraphics();
            g01 = pB01.CreateGraphics();
            g02 = pB02.CreateGraphics();
            g10 = pB10.CreateGraphics();
            g11 = pB11.CreateGraphics();
            g12 = pB12.CreateGraphics();
            g20 = pB20.CreateGraphics();
            g21 = pB21.CreateGraphics();
            g22 = pB22.CreateGraphics();
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            nchg2 = true;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            nchg1 = true;
        }


        private void pB_Click(object sender, EventArgs e)
        {
            //ako je p1 onda pravi O ako ne onda X
            if (player)
            {
                turn_text.Text = p2_rez_name.Text;
                s = new X();
            }
            else
            {
                turn_text.Text = p1__rez_name.Text;
                s = new Circle();
            }

            //promjeni igraca
            player = !player;

            //ako su imena unesena, promjeni ime
            if (nchg1)
                p1__rez_name.Text = player1_name.Text;
            if (nchg2)
                p2_rez_name.Text = player2_name.Text;
        }


        private void pB11_Click(object sender, EventArgs e)
        {
            if (!m11)
            {
                s.Draw(g11, p, e.X, e.Y);
                m11 = !m11;
                if (player) rez11 = 1;
                else rez11 = 2;
            }
            win_check();
        }

        private void pB10_Click(object sender, EventArgs e)
        {
            if (!m10)
            {
                s.Draw(g10, p, e.X, e.Y);
                m10 = !m10;
                if (player) rez10 = 1;
                else rez10 = 2;
            }
            win_check();
        }

        private void pB02_Click(object sender, EventArgs e)
        {
            if (!m02)
            {
                s.Draw(g02, p, e.X, e.Y);
                m02 = !m02;
                if (player) rez02 = 1;
                else rez02 = 2;
            }
            win_check();
        }

        private void pB01_Click(object sender, EventArgs e)
        {
            if (!m01)
            {
                s.Draw(g01, p, e.X, e.Y);
                m01 = !m01;
                if (player) rez01 = 1;
                else rez01 = 2;
            }
            win_check();
        }

        private void pB22_Click(object sender, EventArgs e)
        {

        }

        private void pB20_Click(object sender, EventArgs e)
        {
            if (!m22)
            {
                s.Draw(g22, p, e.X, e.Y);
                m22 = !m22;
                if (player) rez22 = 1;
                else rez22 = 2;
            }
            win_check();
        }

        private void pB21_Click(object sender, EventArgs e)
        {
            if (!m21)
            {
                s.Draw(g21, p, e.X, e.Y);
                m21 = !m21;
                if (player) rez21 = 1;
                else rez21 = 2;
            }
            win_check();
        }

        private void pB12_Click(object sender, EventArgs e)
        {
            if (!m12)
            {
                s.Draw(g12, p, e.X, e.Y);
                m12 = !m12;
                if (player) rez12 = 1;
                else rez12 = 2;
            }
            win_check();
        }

        private void pB00_Click(object sender, EventArgs e)
        {
            if (!m00)
            {
                s.Draw(g00, p, e.X, e.Y);
                m00 = !m00;
                if (player) rez00 = 1;
                else rez00 = 2;
            }
            win_check();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            g00.Clear(Color.White);
            g01.Clear(Color.White);
            g02.Clear(Color.White);
            g10.Clear(Color.White);
            g11.Clear(Color.White);
            g12.Clear(Color.White);
            g20.Clear(Color.White);
            g21.Clear(Color.White);
            g22.Clear(Color.White);


            m00 = false;
            m01 = false;
            m02 = false;
            m10 = false;
            m11 = false;
            m12 = false;
            m20 = false;
            m21 = false;
            m22 = false;

            rez00 = 0;
            rez01 = 0;
            rez02 = 0;
            rez10 = 0;
            rez11 = 0;
            rez12 = 0;
            rez20 = 0;
            rez21 = 0;
            rez22 = 0;

            finished = false;
            
            player = true;
            turn_text.Text = p1__rez_name.Text;

        }

        private void finish()
        {
            m00 = true;
            m01 = true;
            m02 = true;
            m10 = true;
            m11 = true;
            m12 = true;
            m20 = true;
            m21 = true;
            m22 = true;

            finished = true;
        }

        private void win_check()
        {
            if ((rez00 == 1 && rez01 == 1 && rez02 == 1) || (rez10 == 1 && rez11 == 1 && rez12 == 1) || (rez20 == 1 && rez21 == 1 && rez22 == 1) || (rez00 == 1 && rez10 == 1 && rez20 == 1) || (rez01 == 1 && rez11 == 1 && rez21 == 1) || (rez02 == 1 && rez12 == 1 && rez22 == 1) || (rez00 == 1 && rez11 == 1 && rez22 == 1) || (rez02 == 1 && rez11 == 1 && rez20 == 1))
            {
                if (finished == false)
                    O_cnt++;
                finish();
                MessageBox.Show(p1__rez_name.Text + " wins!");
            }
            else if ((rez00 == 2 && rez01 == 2 && rez02 == 2) || (rez10 == 2 && rez11 == 2 && rez12 == 2) || (rez20 == 2 && rez21 == 2 && rez22 == 2) || (rez00 == 2 && rez10 == 2 && rez20 == 2) || (rez01 == 2 && rez11 == 2 && rez21 == 2) || (rez02 == 2 && rez12 == 2 && rez22 == 2) || (rez00 == 2 && rez11 == 2 && rez22 == 2) || (rez02 == 2 && rez11 == 2 && rez20 == 2))
            {
                if (finished == false)
                    X_cnt++;
                finish();
                MessageBox.Show(p2_rez_name.Text + " wins!");
            }
            else if (rez00 != 0 && rez01 != 0 && rez02 != 0 && rez10 != 0 && rez20 != 0 && rez11 != 0 && rez21 != 0 && rez12 != 0 && rez22 != 0)
            {
                if (finished == false)
                    D_cnt++;
                finish();
                MessageBox.Show("Draw!");
            }
            
            p1_rez.Text = O_cnt.ToString();
            p2_rez.Text = X_cnt.ToString();
            draw_rez.Text = D_cnt.ToString();
        }
    
    }
}
